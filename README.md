# erpnext_oob_docker

#### 介绍
本docker镜像是在官方镜像基础上安装了中文汉化，开箱即用，权限优化与敏捷制造，可根据需要增减自定义应用

### 前提条件

1. linux系统
2. 已安装好docker及docker-compose，参考版本号：Docker version 20.10.18, build b40c2f6，Docker Compose version v2.6.1，太低版本可能需要升级或重装
```
docker -v 
docker-compose -v
```
3. docker服务已启动，且当前用户需有docker组，即可直接运行docker命令不报错，可用以下命令添加当前用户到docKer组

```
sudo groupadd docker
$ sudo gpasswd -a $USER docker
$ newgrp docker
```



### 准备工作：下载构建与部署命令及参数文件，并切换到工作目录

```
git clone https://gitee.com/yuzelin/erpnext_oob_docker
cd erpnext_oob_docker
```

### 构建镜像(可选步骤)

`backend.Dockerfile`和`frontend.Dockerfile`已包括了中文汉化，开箱即用，权限优化与敏捷制造四个应用，可根据需要删除和添加自定义应用

```bash
cd build

# 指定基础镜像及目标镜像版本并开始构建,可在hub.docker.com搜frappe关键字找到可用镜像版本号，
# FRAPPE_VERSION 匹配 https://hub.docker.com/r/frappe/assets-builder/tags
# ERPNEXT_VERSION 匹配 https://hub.docker.com/r/frappe/erpnext-worker/tags 和
# https://hub.docker.com/r/frappe/erpnext-nginx/tags,可以放具体小版本，但需两个基础镜像都有相同版本号
# 其中VERSION=v13.39.0.221022号修改之后，需在部署步骤中相应修改.env中对应的参数

FRAPPE_VERSION=v13 \
ERPNEXT_VERSION=v13 \
VERSION=v13_221023 \
docker buildx bake
```

### 部署服务

修改`.env`环境变量(可选步骤)
如果有域名，请在.env 文件中参数 `FRAPPE_SITE_NAME_HEADER=erpnext13.local` 中的erpnext13.local为域名，即保持与以下docker compose exec backend bench new-site erpnext13.local一致。

```bash
# 如果从上面build目录切换，请使用 cd ../deploy
cd deploy
# 启动docker-compose.yaml中定义的9个ERPNext服务，替代传统生产模式中的supervisorctl进程管理
# 如果不做前面的构建步骤，本步骤直接下载镜像，运行时间约12分钟
docker compose up -d

# 初次启动新建站点并安装镜像中已加载的app(第一次启动才执行)，运行时间约 5 分钟
# new-site <domain>：将erpnext13.local修改为自己的网站域名
# mariadb-root-password 修改数据库密码，需与上述环境变量文件.env中的密码一致
# admin-password 修改使用administrator帐号初次登录系统时的密码

docker compose exec backend \
  bench new-site erpnext13.local \
    --mariadb-root-password 123 \
    --admin-password admin \
    --install-app erpnext \
    --install-app erpnext_chinese \
    --install-app erpnext_oob \
    --install-app zelin_permission \
    --install-app zelin_am

# 新建站点后需要重启后端
docker compose restart backend
```

#### 使用系统
在用户电脑上打开浏览器, 输入域名:8080或IP:8080 用户名administrator,密码admin登录系统

#### 使用外部数据库和Redis

可在`.env`中设置对应的数据库或Redis的环境变量来指定外部服务，指定外部服务后可以停止或删除docker-compose.yml中的`db`或`redis`

### 更新版本

修改`.env`环境变量`VERSION`为新镜像版本

```bash
# 停止服务
docker compose down

# (再次) 启动服务
docker compose up -d
```

```bash
# 涉及数据库结构变更以及hooks勾子中变更了后台任务后，需执行bench migrate
docker compose exec backend bench --site erpnext13.local migrate

# 针对多个改动可能需要重启后端
docker compose restart backend
```

 ** 常见问题** 
1. 如果build(构建)时出现以下问题，当前docker及docker-compose版本不正确，建议卸载后重新安装
=> ERROR [frontend internal] load metadata for docker.io/frappe/erpnext-nginx:v13                                                1.5s
 => ERROR [frontend internal] load metadata for docker.io/frappe/assets-builder:v13                                               1.3s
 => ERROR [backend] resolve image config for docker.io/docker/dockerfile:1.3 
2. docker compose up -d 速度比较慢
可设置国内镜像源
2.1 新增或修改 /etc/docker/daemon.json，增加以下内容

```
{
  "registry-mirrors": ["https://registry.docker-cn.com"]
}
```

2.2 重启docker

```
sudo systemctl daemon-reload
sudo systemctl restart docker
```

附： ubuntu docker及docker compose 安装方法
https://blog.csdn.net/endswell/article/details/126632613

docker-compose安装
切换到root帐号

```
sudo -i 
curl -L https://get.daocloud.io/docker/compose/releases/download/v2.6.1/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```


debian 安装docker
https://blog.csdn.net/qq_46416934/article/details/126580402