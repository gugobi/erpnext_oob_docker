# syntax=docker/dockerfile:1.3

ARG ERPNEXT_VERSION
FROM frappe/erpnext-worker:${ERPNEXT_VERSION}

USER root
WORKDIR /home/frappe/frappe-bench/sites

copy configure.py /usr/local/bin/configure.py
RUN chmod 0755 /usr/local/bin/configure.py

RUN --mount=type=cache,target=/root/.cache/pip \
  sed -i "s@http://\(deb\|security\).debian.org@https://mirrors.ustc.edu.cn@g" /etc/apt/sources.list && \
  apt update && \
    # 解决PDF中文乱码问题
  apt-get install ttf-wqy-zenhei -y && \
  apt-get install ttf-wqy-microhei -y && \
  apt install git -y && \
  cd ../apps && \
  # 安装app
  git clone https://gitee.com/yuzelin/erpnext_chinese.git --depth=1 && \
  install-app erpnext_chinese && \
  git clone https://gitee.com/yuzelin/erpnext_oob.git --depth=1 && \
  install-app erpnext_oob && \
  # 自定以app
  git clone https://gitee.com/yuzelin/zelin_permission.git --depth=1 && \
  install-app zelin_permission && \
  git clone https://gitee.com/yuzelin/zelin_am.git --depth=1 && \
  install-app zelin_am

USER frappe