ARG FRAPPE_VERSION
ARG ERPNEXT_VERSION

FROM frappe/assets-builder:${FRAPPE_VERSION} as assets

RUN cd apps && \
  git clone https://gitee.com/yuzelin/erpnext_chinese.git --depth=1 && \
  # install-app要求app要有{app_name}/{module_name}/public目录，否则安装失败
  # 如果项目没有{app_name}/{module_name}/public目录，请新建一个
  mkdir -p erpnext_chinese/erpnext_chinese/public && \
  install-app erpnext_chinese && \
  \
  git clone https://gitee.com/yuzelin/erpnext_oob.git --depth=1 && \
  mkdir -p erpnext_oob/erpnext_oob/public && \
  install-app erpnext_oob && \
  \
  # 自定义app
  git clone https://gitee.com/yuzelin/zelin_permission.git --depth=1 && \
  mkdir -p zelin_permission/zelin_permission/public && \
  install-app zelin_permission && \
  \
  git clone https://gitee.com/yuzelin/zelin_am.git --depth=1 && \
  mkdir -p zelin_am/zelin_am/public && \
  install-app zelin_am

FROM frappe/erpnext-nginx:${ERPNEXT_VERSION}

COPY --from=assets /out /usr/share/nginx/html